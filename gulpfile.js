const path = require('path')
const sass = require('gulp-sass');
const gulpStylelint = require('gulp-stylelint');
const prettier = require('gulp-prettier');
const tslint = require('gulp-tslint');
const ts = require("gulp-typescript");
const tsProj = ts.createProject('./tsconfig.json');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');
const fileinclude = require('gulp-file-include');
const webserver = require('gulp-webserver');
const clean = require('gulp-clean');
const gulp = require('gulp');


gulp.task('styles', function () {
  return gulp
    .src('./src/styles/*.scss')
    .pipe(gulpStylelint({
        fix: true,
        failAfterError: false,
        reporters: [{ formatter: 'verbose', console: true }]
      })
    )
    .pipe(gulp.dest('./src/styles/'))
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('script', function () {
  return gulp
    .src('./src/**/*.ts')
    .pipe(prettier({ singleQuote: true }))
    .pipe(gulp.dest(file => file.base))
    .pipe(tslint({ formatter: 'stylish', configuration: './tslint.json', fix: true }))
    .pipe(tslint.report({
      emitError: false
    }))
    .pipe(sourcemaps.init())
      .pipe(tsProj())
      .js
      .pipe(babel({"presets": ["minify"]}))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./dist/'))
});

gulp.task('fileinclude', function () {
  return gulp.src(['./src/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./dist'));
});
gulp.task('clean', function() {
  return gulp.src('./dist/')
    .pipe(clean())
})
gulp.task('serve', function() {
  return gulp.src('./dist')
    .pipe(webserver({
      livereload: true,
      open: '/home.html'
    }));
});


gulp.task('build', gulp.parallel('script', 'fileinclude', 'styles'));


gulp.task('watch', gulp.series('clean', 'build', 'serve'), function(){
  gulp.watch('./src/styles/*.scss', ['styles']);
  gulp.watch('./src/script/*.ts', ['script']);
  gulp.watch('./src/html/**/*.html', ['fileinclude']);
})